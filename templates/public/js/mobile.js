$(document).ready(function () {

    $('.hamburger #a').on('click', function () {
        if ($(this).hasClass('isopen')) {
            $(this).html('<i class="fa fa-bars"></i>').addClass('isclose').removeClass('isopen');
            event.preventDefault();
            $('.mobile-menu').fadeOut("slow", function () {

            })
            $('body').css({
                "overflow": "auto"
            })
        }

        else {
            event.preventDefault();
            $(this).html('<i class="fa fa-close"></i>').addClass('isopen').removeClass('isclose');
            $('.hamburger').css({
                'z-index': 3
            })
            $('.mobile-menu').fadeIn("slow", function () {

            });
            $('body').css({
                "overflow": "hidden"
            })
        }
    });
    $('.hamburger-menu #a').on('click', function () {

        $('.mobile-menu').fadeOut("slow", function () {

        })
        $('body').css({
            "overflow": "auto"
        })
    });
    if (($(document).scrollTop() > 1)) {
        $('.hamburger').css({
            'background-color': 'rgba(0,0,0,0.7)'
        })
    }
    else {
        $('.hamburger').css({
            'background-color': 'transparent'
        })
    }
    $(document).scroll(function () {

        if (($(document).scrollTop() > 1)) {
            $('.hamburger').css({
                'background-color': 'rgba(0,0,0,0.5)'
            })
        }
        else {
            $('.hamburger').css({
                'background-color': 'transparent'
            })
        }
    })
});