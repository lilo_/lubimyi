import csv, sys, os
from app_models.models import Post
import django

django.setup()
# project_dir = "/PycharmProjects/lubimyi2/"

# sys.path.append(project_dir)

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
currentdirpath = os.getcwd()
file_name = 'data.csv'
file_path = os.path.join(os.getcwd(),file_name)
data = csv.reader(open("\data.csv"), delimiter=",")

for row in data:
    if row[0] != 'create_date':
        post = Post()
        post.create_date = row[0]
        post.tittle = row[1]
        post.time_begin = row[2]
        post.time_end = row[3]
        post.save()
