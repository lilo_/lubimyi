"""Lubimyi2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from app.views import *

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^$', index, name='index'),
    url(r'^contacts/$', contacts, name='contacts'),
    url(r'^adders/$', adders, name='adders'),
    url(r'^adder/(?P<id>\d+)$$', adders_single, name='adders_single'),
    url(r'^tv_project/(?P<id>\d+)$', tv_project_single, name='tv_project_single'),
    url(r'^table/$', table, name='table'),
    url(r'^projects/$', projects, name='projects'),
    url(r'^about_us/$', about_us, name='about_us'),
    url(r'^portfolio/$', portfolio, name='portfolio'),
    url(r'^add/$', add, name='add'),
    url(r'page/(\d+)/$', portfolio, name='portfolio_page'),
    url(r'^copy_items', copy_items, name='copy_items'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
