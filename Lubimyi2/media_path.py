# -*- coding: utf-8 -*-
from pytils import translit
import uuid


def file_path(self, filename):
    # file will be uploaded to MEDIA_ROOT/banners/images/<filename>
    return u'media/ecs/%s/%s_%s' % (self.owner.id, str(uuid.uuid4().hex[2:4]), translit.translify(filename))
