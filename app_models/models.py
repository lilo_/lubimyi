# coding=utf-8
from __future__ import unicode_literals
import csv
from Lubimyi2.media_path import *
from sorl.thumbnail import ImageField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
from Lubimyi2.settings import MEDIA_ROOT
from app_models.helper import transform

pathFile = 'posts/file'
pathImg = 'images'


class Banner(models.Model):
    class Meta:
        db_table = 'banner'
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'

    title = models.CharField(max_length=255, verbose_name='Наименование', blank=True, null=True)
    short_description = models.CharField(max_length=150, verbose_name='Краткое описание', blank=True, null=True)
    date = models.CharField(max_length=255, verbose_name='дата', default='', blank=True)
    # description = RichTextField(verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title


class Contacts(models.Model):
    class Meta:
        db_table = 'contacts'
        verbose_name = 'контакт'
        verbose_name_plural = 'контакты'

    mail = models.EmailField(verbose_name='E-mail')
    name = models.CharField(max_length=255, verbose_name='Имя')
    number = PhoneNumberField(verbose_name='НОмер телефона')
    comment = RichTextUploadingField()

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.name


class Adders(models.Model):
    class Meta:
        db_table = 'adders'
        verbose_name = 'Рекламодателям'
        verbose_name_plural = 'Рекламодателям'

    title = models.CharField(max_length=255, verbose_name='Название')
    description = RichTextUploadingField(verbose_name='Описание')
    image = ImageField(upload_to=transform(pathImg), verbose_name='Изображение')

    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title


class Videos(models.Model):
    class Meta:
        db_table = 'videos'
        verbose_name = 'видео'
        verbose_name_plural = 'видео'

    title = models.CharField(max_length=255, verbose_name='Название')
    # link = models.URLField(default='')
    url = models.CharField(max_length=100, verbose_name='ссылка', blank=True,
                           help_text='не вся ссылка, только ключ видео')
    image = ImageField(upload_to=transform(pathImg), verbose_name='Изображение')

    date = models.DateTimeField(auto_now_add=True, verbose_name='Дата')
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title


class Post(models.Model):
    class Meta:
        db_table = 'table'
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'

    # date = models.CharField(verbose_name='дата', default='', max_length=20)
    date_char = models.DateField(verbose_name='Дата')
    title = models.CharField(max_length=250, verbose_name='Наименование')
    type = models.CharField(max_length=250, verbose_name='Жанр')
    time_begin = models.TimeField(verbose_name='Время начала', blank=True, null=True)
    # during_time = models.DurationField(verbose_name='длительность', null=True)
    time_end = models.TimeField(verbose_name='Время завершения', null=True)

    # timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    # updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title

        # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        #     time_end = self.during_time,self.time_begin
        #     print time_end
        #     self.time_end = time_end
        #     super(Post, self).save()


class Tv_projects(models.Model):
    class Meta:
        db_table = 'tv_projects'
        verbose_name = 'телепроект'
        verbose_name_plural = 'телепроекты'

    title = models.CharField(max_length=255, verbose_name='название')
    short_description = models.CharField(max_length=1000, verbose_name='Краткое описание')
    description = RichTextUploadingField(verbose_name='Описание')
    image = models.ImageField(upload_to=transform(pathImg), verbose_name='Изображение')

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title


class File(models.Model):
    class Meta:
        db_table = 'file'
        verbose_name = 'Файл'
        verbose_name_plural = 'Файлы'

    title = models.TextField(max_length=100)
    file = models.FileField(upload_to=transform(pathFile))

    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.title


@receiver(post_save, sender=File, dispatch_uid='File_Reader')
def save_table(sender, instance, *args, **kwargs):
    # File.objects.all().delete()
    # Post.objects.all().delete()

    with open(MEDIA_ROOT + '/' + str(instance.file)) as f:
        reader = csv.reader(f, delimiter=str(u'\t').encode('utf-8'), skipinitialspace=True)
        for cols in reader:
            old_date = cols[0].split('.')
            table = Post()
            new_date = old_date[2] + '-' + old_date[1] + '-' + old_date[0]
            table.date_char = new_date
            table.time_begin = cols[1]
            table.time_end = cols[2]
            # table.time_end = cols[3]
            table.title = str(cols[3]).decode("cp1251")
            if table.type:
                table.type = str(cols[4]).decode("cp1251")
            else:
                table.type = ''
            table.save()
