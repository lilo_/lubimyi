# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-10-19 14:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='time_begin',
            field=models.TimeField(blank=True, null=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043d\u0430\u0447\u0430\u043b\u0430'),
        ),
    ]
