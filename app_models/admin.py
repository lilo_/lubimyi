from django.contrib import admin
from models import *


# Register your models here.

#
class FileAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.DateTimeField: {'input_formats': ('%m/%d/%Y',)},
    }


class ContactsAdmin(admin.ModelAdmin):
    list_display = 'name mail number comment'.split()
    list_filter = ['name', 'mail']
    # list_editable = ['mail']


# class ProgramAdmin(admin.ModelAdmin):
#     list_display = 'step_title time_begin time_finish'.split()
#     list_editable = ['time_begin', 'time_finish']
#     list_filter = ['step_title', 'time_begin', 'time_finish']


class PostAdmin(admin.ModelAdmin):
    list_display = 'step_title time_begin time_finish'.split()
    list_editable = ['time_begin', 'time_finish']
    list_filter = ['step_title', 'time_begin', 'time_finish']


# class BookResource(resources.ModelResource):
#     class Meta:
#         model = Post
#         list_display = 'title time_begin time_end date'.split()
#         list_editable = ['time_begin', 'time_end']
#         list_filter = ['title', 'time_begin', 'time_end']
#         # exclude = ('id',)
#         # import_id_fields = ['date', 'title', 'time_begin', 'time_end']
#
#
# class BookAdmin(ImportExportModelAdmin):
#     resource_class = BookResource

class PostDateFieldAdmin(admin.ModelAdmin):
    def date_char(self, obj):
        return obj.datefield.strftime("%d.%m.%Y")

    date_char.admin_order_field = 'datefield'
    date_char.short_description = 'Precise Time'
    list_editable = ['time_begin', 'time_end', 'type', 'title']

    list_display = ('date_char', 'title', 'time_begin', 'time_end', 'type')
    list_filter = ('date_char', 'title', 'time_begin', 'time_end', 'type')


admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Banner)
admin.site.register(Adders)
admin.site.register(Videos)
# admin.site.register(Table)
# admin.site.register(Program, ProgramAdmin)
admin.site.register(Tv_projects)
admin.site.register(Post, PostDateFieldAdmin)
admin.site.register(File, FileAdmin)
