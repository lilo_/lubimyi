from django.forms import ModelForm, DateField

from Lubimyi2 import settings
from app_models.models import *


class ClientDetailsForm(ModelForm):
    date_of_birth = DateField(input_formats=settings.DATE_INPUT_FORMATS)

    class Meta:
        model = Post
