from datetime import datetime

from django import template

register = template.Library()


@register.simple_tag
def show_link(page, num_pages, current_page):
    page = int(page)
    num_pages = int(num_pages)
    current_page = int(current_page)

    if page == current_page:
        return True
    if (page <= 5 and current_page <= 5) or (page >= num_pages - 5 and current_page >= num_pages - 5):
        return True
    if page == 1 or page == num_pages:
        return True
    if current_page - page in [1, 2] or page - current_page in [1, 2]:
        return True

    return False


@register.assignment_tag
def set(value):
    return value


@register.filter
def get_value_by_key(dict, key):
    return dict[key]
