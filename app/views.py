# coding=utf-8
from collections import OrderedDict

from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import render

from Lubimyi2 import settings
from app_models.forms import ContactForm
import datetime
from app_models.models import *


# import numpy as np
# y = np.loadtxt("table.txt")
# print y

# Create your views here.

def index(request):
    banner = Banner.objects.all().order_by('id')
    tv_project = Tv_projects.objects.all().order_by('id')
    post = Post.objects.all()
    now = datetime.datetime.now().time()
    today = datetime.datetime.now().today().date()
    left_column = today + datetime.timedelta(days=2)
    table_list = Post.objects.filter(date_char__gte=today, date_char__lte=left_column).order_by('date_char')

    params = {
        'banner': banner,
        'now': now,
        'tv_project': tv_project,
        'post': post,
        'today': today,
        'left_column': left_column,
        'today_list': table_list
    }

    return render(request, 'app/index.html', params)


def tv_project_single(request, id):
    tv_project = Tv_projects.objects.get(id=id)
    params = {
        'tv_project': tv_project,
    }
    return render(request, 'app/misc/tv_project_single.html', params)


def contacts(request):
    contact_form = ContactForm(request.POST)
    if request.POST:
        if contact_form.is_valid():
            c = Contacts()
            c.name = contact_form.cleaned_data['name']
            c.mail = contact_form.cleaned_data['mail']
            c.number = contact_form.cleaned_data['number']
            c.comment = contact_form.cleaned_data['comment']
            c.save()
            return JsonResponse(dict(success=True, message='Успешно!!'))
        return JsonResponse(dict(success=False, message='Форма не валидна'))

    params = {
        'form': contact_form,
    }
    return render(request, 'app/contacts.html', params)


def adders(request):
    adders = Adders.objects.all().order_by('id')[:12]
    params = {
        'adders': adders,
    }
    return render(request, 'app/adders.html', params)


def adders_single(request, id):
    adders = Adders.objects.get(id=id)
    params = {
        'adder': adders,
    }
    return render(request, 'app/misc/adders_single.html', params)


def about_us(request):
    params = {

    }
    return render(request, 'app/about_us.html', params)


def portfolio(request, page_number=1):
    video_list = Videos.objects.all().order_by('-date')
    current_page = Paginator(video_list, 12)
    # page = request.GET.get('page', default='1')

    params = {
        'videos': current_page.page(page_number)
    }
    return render(request, 'app/portfolio.html', params)


def projects(request):
    tv_project = Tv_projects.objects.all().order_by('id')[:10]
    params = {
        'tv_project': tv_project,
    }
    return render(request, 'app/projects.html', params)


def table(request):
    post = Post.objects.all()
    now = datetime.datetime.now().time()
    today = datetime.datetime.now().date()
    today_temp = today
    yesterday = datetime.datetime.now().date() - datetime.timedelta(days=1)
    tomorrow = datetime.datetime.now().date() + datetime.timedelta(days=1)
    table = today + datetime.timedelta(days=7)
    table_list = Post.objects.filter(date_char__gte=today, date_char__lte=table).order_by('date_char')


    params = {
        'post': post,
        'today': today,
        'yesterday': yesterday,
        'tomorrow': tomorrow,
        'now': now,
        'table':table,
        'table_list': table_list,

    }
    return render(request, 'app/table.html', params)


def add(request):
    add = Adders.objects.all().order_by('id')[:12]
    params = {
        'add': add,
    }
    return render(request, 'app/misc/add.html', params)


def copy_items(request):
    obj = Post.objects.get(pk=1)
    for i in range(25):
        new_obj = obj
        new_obj.pk = None
        obj.date_char = datetime.datetime.now().date()
        new_obj.save()

# def get_file_path(filename):
#     currentdirpath = os.getcwd()
#     file_path = os.path.join(os.getcwd(), filename)
#     print file_path
#     return file_path
#
#
# path = get_file_path('data.csv')
#
#
# def read_csv(filepath):
#     with open(filepath, 'rU') as csvfile:
#         reader = csv.reader(csvfile)
#         print reader
#         for row in reader:
#             post = Post()
#             post.create_date = row[0]
#             post.title = row[1]
#             post.time_begin = row[2]
#             post.time_end = row[3]
#             post.save()
#
#             print row
#
#
# read_csv(path)
#
